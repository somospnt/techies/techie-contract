package com.somospnt.contractproducer.web.controller.rest;

import com.somospnt.contractproducer.domain.Cliente;
import com.somospnt.contractproducer.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("/cliente/{id}")
    public Cliente buscarCliente(@PathVariable("id") Long id) {
        return clienteRepository.buscarPorId(id);
    }
}
