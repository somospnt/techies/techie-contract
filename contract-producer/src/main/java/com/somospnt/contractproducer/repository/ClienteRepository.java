package com.somospnt.contractproducer.repository;

import com.somospnt.contractproducer.domain.Cliente;
import java.util.NoSuchElementException;
import org.springframework.stereotype.Repository;

@Repository
public class ClienteRepository {

    public Cliente buscarPorId(Long id) {
        if (id == 1) {
            return Cliente.builder().id(1L).nombre("marta").build();
        }
        throw new NoSuchElementException();
    }

}
