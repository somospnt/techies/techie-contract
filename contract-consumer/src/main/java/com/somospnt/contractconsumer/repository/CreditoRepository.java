package com.somospnt.contractconsumer.repository;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import org.springframework.stereotype.Repository;

@Repository
public class CreditoRepository {

    public BigDecimal buscarCreditoPorClienteId(Long clienteId) {
        if (clienteId == 1) {
            return BigDecimal.TEN;
        }
        throw new NoSuchElementException();
    }

}
