package com.somospnt.contractconsumer.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cliente {

    public Cliente() {
    }

    private Long id;
    private String nombre;

}
