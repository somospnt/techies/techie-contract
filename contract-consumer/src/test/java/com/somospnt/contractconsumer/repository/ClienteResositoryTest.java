package com.somospnt.contractconsumer.repository;

import com.somospnt.contractconsumer.ContractConsumerApplicationTests;
import com.somospnt.contractconsumer.domain.Cliente;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.web.client.HttpClientErrorException;

@AutoConfigureStubRunner(
        stubsMode = StubRunnerProperties.StubsMode.LOCAL,
        ids = "com.somospnt:contract-producer:0.0.1-SNAPSHOT:stubs:8090")
public class ClienteResositoryTest extends ContractConsumerApplicationTests {

    @Autowired
    private ClienteRepository clieteRepository;

    @Test
    public void buscarPorId_clienteExistente_retornaCliente() throws Exception {
        Cliente cliente = clieteRepository.buscarPorId(1L);
        assertNotNull(cliente);
        assertNotNull(cliente.getId());
        assertNotNull(cliente.getNombre());
    }

    @Test
    public void buscarPorId_clienteNoExistente_lanzaExcecion() throws Exception {
        Assertions.assertThrows(HttpClientErrorException.class, () -> clieteRepository.buscarPorId(999L));        
    }

}
